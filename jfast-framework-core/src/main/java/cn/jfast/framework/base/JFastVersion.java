/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.base;

/**
 * JFast SDK版本号声明
 */
public class JFastVersion {

    /**
     * 获取 JFast 的版本号，版本号的命名规范
     *
     * <pre>
     * [大版本号].[质量号].[发布流水号]
     * </pre>
     *
     * @return jfast.version 项目的版本号
     */
    public static String version() {
        return String.format("%d.%s.%d", majorVersion(), releaseLevel(), minorVersion());
    }

    public static int majorVersion() {
        return 1;
    }

    public static int minorVersion() {
        return 1;
    }

    public static String releaseLevel() {
        //a: 内部测试品质, b: 公测品质, r: 最终发布版
        return "1";
    }
}
