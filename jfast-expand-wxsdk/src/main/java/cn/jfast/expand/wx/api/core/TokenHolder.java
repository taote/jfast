package cn.jfast.expand.wx.api.core;

import cn.jfast.expand.wx.api.msg.MPAct;

public interface TokenHolder {
	
	void setAccessToken(MPAct mpAct,String accessToken);
	
	String getAccessToken(MPAct mpAct);
	
}
