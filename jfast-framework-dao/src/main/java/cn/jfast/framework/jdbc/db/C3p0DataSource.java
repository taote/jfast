/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.jdbc.db;

import java.beans.PropertyVetoException;
import java.sql.SQLException;

import javax.sql.DataSource;

import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.LogType;
import cn.jfast.framework.log.Logger;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * C3p0数据源
 *
 * @author 泛泛o0之辈
 */
public class C3p0DataSource implements IDataSource {

	private ComboPooledDataSource dataSource;
	
	private Logger log = LogFactory.getLogger(LogType.JFast, C3p0DataSource.class);
	
	/**
	 * 数据源初始化
	 * @return
	 */
	public boolean init() {
		dataSource = new ComboPooledDataSource();
		dataSource.setJdbcUrl(DBProp.getJdbcUrl());
		dataSource.setUser(DBProp.jdbc_user);
		dataSource.setPassword(DBProp.jdbc_password);
		try {
			dataSource.setDriverClass(DBProp.jdbc_driver);
		} catch (PropertyVetoException e) {
			log.error("", e);
		}
		dataSource.setMaxPoolSize(DBProp.max_pool_size);
		dataSource.setMinPoolSize(DBProp.min_pool_size);
		dataSource.setInitialPoolSize(DBProp.initial_pool_size);
		dataSource.setMaxIdleTime(DBProp.max_idle_time);
		dataSource.setAcquireIncrement(DBProp.acquire_increment);
		return true;
	}
    
	/**
	 * 获得数据源
	 */
	public DataSource getDataSource() {
		return dataSource;
	}
	
	/**
	 * 关闭数据源
	 * @return
	 * @throws SQLException 
	 */
	public boolean stop() throws SQLException {
		if (dataSource != null)
			dataSource.close();
		return true;
	}
}
