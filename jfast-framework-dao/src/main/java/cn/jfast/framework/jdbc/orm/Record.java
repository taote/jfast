package cn.jfast.framework.jdbc.orm;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.LinkedHashMap;

public class Record extends LinkedHashMap<String,Object>{
	
	private static final long serialVersionUID = 526989347171006182L;
	
	public <T> T entity(Class<T> clazz){  //该转换比较简单只适用于Jfast Dao操作的 Record类
		T entity = null;
		try {
			entity = clazz.newInstance();
			Field[] fields = null;  
		    fields = clazz.getDeclaredFields();  
			for (Field field : fields) {  
				field.setAccessible(true);
			   int modifier = field.getModifiers();  
			   if(Modifier.isStatic(modifier) || Modifier.isFinal(modifier)) 
				   continue;  
			    if(this.containsKey(field.getName().toLowerCase())){
			    	field.set(entity, get(field.getName()));
			    } else {
			    	MapLoop:for(String key:this.keySet()){
			    		if(key.replaceAll("\\W|_", "").toLowerCase().equals(field.getName().replaceAll("\\W|_", "").toLowerCase())){
			    			field.set(entity, get(key));
			    			break MapLoop;
			    		}
			    	}
			    }
			 }  
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return entity;
	}
	
}
