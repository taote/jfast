/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.view.viewtype;

import cn.jfast.framework.web.view.View;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.List;
import java.util.Map;

public class Json extends View {
    private String json;

    public Json(List<?> json) {
    	if(!(json instanceof JSONArray))
    		this.json = JSONArray.fromObject(json).toString();
    	else
    		this.json = ((JSONArray)json).toString();
    }

    public Json(Map<?,?> json) {
    	if(!(json instanceof JSONObject))
    		this.json = JSONObject.fromObject(json).toString();
    	else
    		this.json = ((JSONObject)json).toString();
    }

    public Json(String json) {
        this.json = json;
    }
    
    public Json(Object obj){
    	this.json = JSONObject.fromObject(obj).toString();
    }

    @Override
    public String getView() {
        return this.json.replaceAll("null", "\"\"");
    }

    @Override
    public String toString() {
        return "json:" + this.getView();
    }

}
